#ifndef SRC_S21_MATRIX_H_
#define SRC_S21_MATRIX_H_

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define SUCCESS 1
#define FAILURE 0

typedef enum { OK, INCORRECT_MATRIX, INCORRECT_COUNT } TResult;
typedef enum { FALSE, TRUE } TBool;

typedef struct matrix_struct {
  double **matrix;
  int rows;
  int columns;
} matrix_t;

/* ОСНОВНЫЕ ФУНКЦИИ */
int s21_create_matrix(int rows, int columns, matrix_t *result);
void s21_remove_matrix(matrix_t *A);
int s21_eq_matrix(matrix_t *A, matrix_t *B);

int s21_sum_matrix(matrix_t *A, matrix_t *B, matrix_t *result);
int s21_sub_matrix(matrix_t *A, matrix_t *B, matrix_t *result);
int s21_mult_number(matrix_t *A, double number, matrix_t *result);
int s21_mult_matrix(matrix_t *A, matrix_t *B, matrix_t *result);

int s21_transpose(matrix_t *A, matrix_t *result);
int s21_calc_complements(matrix_t *A, matrix_t *result);
int s21_determinant(matrix_t *A, double *result);
int s21_inverse_matrix(matrix_t *A, matrix_t *result);

/* ВСПОМОГАТЕЛЬНЫЕ ФУНКЦИИ */
double s21_determinant_count(matrix_t *A);

TBool is_matrix_correct(matrix_t *matrix);
TResult minor(int row, int column, matrix_t *A, matrix_t *result);
TResult calc_minor_matrix(matrix_t *A, matrix_t *result);

#endif  // SRC_S21_MATRIX_H_